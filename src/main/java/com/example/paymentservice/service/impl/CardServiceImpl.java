package com.example.paymentservice.service.impl;

import com.example.paymentservice.client.PaymentClient;
import com.example.paymentservice.entity.Card;
import com.example.paymentservice.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

    private final PaymentClient paymentClient;

    @Override
    public String pay(Card card) {
        return paymentClient.pay(card);
    }
}
