package com.example.paymentservice.controller;

import com.example.paymentservice.entity.Card;
import com.example.paymentservice.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/pay")
@RequiredArgsConstructor
public class CardController {

    private final CardService cardService;

    @PostMapping("/payment")
    public String pay(@RequestBody Card card){
        cardService.pay(card);
        return "payment successfully";
    }

    @GetMapping()
    public String helloWorld1(){
        return "hello";
    }
}
