package com.example.paymentservice.service;

import com.example.paymentservice.entity.Card;

public interface CardService {

    String pay(Card card);
}
