package com.example.paymentservice.client;

import com.example.paymentservice.entity.Card;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "kb-service",url = "localhost:8081/api/pay/capitalBank")
public interface PaymentClient {

    @PostMapping
    String pay(@RequestBody Card card);


}
